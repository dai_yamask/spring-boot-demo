package com.example.springbootdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class Hello {

    @GetMapping("/hello")
    public Mono<String> hello(@RequestParam(name = "name", defaultValue = "spring") String name) {
        return Mono.just("hello " + name);
    }

}
