package com.example.springbootdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.jupiter.api.Assertions.*;

@WebFluxTest(Hello.class)
class HelloTest {

    @Autowired
    WebTestClient client;

    @Test
    void hello() {
        client.get()
                .uri("/hello")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class).isEqualTo("hello spring");
    }

    @Test
    void helloWithName() {
        client.get()
                .uri("/hello?name=Yamasaki")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class).isEqualTo("hello Yamasaki");
    }
}
